package main

import (
	"fmt"
	"os"

	"encoding/hex"

	"github.com/cosmos/cosmos-sdk/types/bech32"
)

func main() {
	address := os.Args[1]
	s, b, err := bech32.DecodeAndConvert(address)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("> prefix:", s)
	fmt.Println("> bytes:", b)

	h := "0x" + hex.EncodeToString(b)
	fmt.Println("> bytes32 format:", h)
}
