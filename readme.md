# Cosmos <> Ethereum bridge blockchain

## What is a zone ?

A **zone** is a blockchain built using Cosmos SDK and Tendermint and created with [Starport](https://github.com/tendermint/starport).


## Get started

Build the cli and deamon of the zone:

```
starport serve
```

`serve` command installs dependencies, builds, initializes and starts your blockchain in development.


## Launch your blockchain node

Notice that in starport, cli and daemon are in the same command.

Here the command scaffold by `starport serve` is: `zoned`.

```
zoned start
```

## Use the cli

Cosmos SDK works in modules.

### For queries

```
zoned query {module_of your_choice} {command_of_your_choice}
```

### For transactions

```
zoned tx {module_of your_choice} {command_of_your_choice}
```
