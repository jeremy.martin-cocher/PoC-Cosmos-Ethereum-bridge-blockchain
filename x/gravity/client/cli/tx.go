package cli

import (
	"crypto/ecdsa"
	"encoding/hex"
	"fmt"
	"log"
	"strconv"

	"github.com/cosmos/cosmos-sdk/client"
	"github.com/cosmos/cosmos-sdk/client/flags"
	"github.com/cosmos/cosmos-sdk/client/tx"
	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkerrors "github.com/cosmos/cosmos-sdk/types/errors"
	ethCrypto "github.com/ethereum/go-ethereum/crypto"
	"github.com/spf13/cobra"

	"github.com/jmc171144/zone/x/gravity/types"
)

func GetTxCmd(storeKey string) *cobra.Command {
	gravityTxCmd := &cobra.Command{
		Use:                        types.ModuleName,
		Short:                      "Gravity transaction subcommands",
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}

	gravityTxCmd.AddCommand([]*cobra.Command{
		CmdSendToEth(),
		CmdRequestBatch(),
		CmdSetOrchestratorAddress(),
		GetUnsafeTestingCmd(),
		CmdValsetConfirm(),
		CmdConfirmBatch(),
	}...)

	return gravityTxCmd
}

//MsgERC20DeployedClaim
// func CmdERC20DeployedClaim() *cobra.Command {
// 	cmd := &cobra.Command{
// 		Use:   "erc20-deployed-claim [nonce] [eth-block-height] [demon] [contract] [name] [symbol] [decimals] [orchestrator]",
// 		Short: "Confirm a specific batch",
// 		Args:  cobra.ExactArgs(8),
// 		RunE: func(cmd *cobra.Command, args []string) error {
// 			cliCtx, err := client.GetClientTxContext(cmd)
// 			if err != nil {
// 				return err
// 			}

// 			nonceValue, err := strconv.ParseUint(args[0], 10, 64)
// 			if err != nil {
// 				return sdkerrors.Wrap(err, "nonce conversion")
// 			}

// 			blockHeight, err := strconv.ParseUint(args[1], 10, 64)
// 			if err != nil {
// 				return sdkerrors.Wrap(err, "nonce conversion")
// 			}

// 			//---------------------- 4. Send msg ----------------------
// 			msg := types.MsgERC20DeployedClaim{
// 				EventNonce:         nonceValue,
// 				BlockHeight: 		blockHeight,
// 				TokenContract: args[1],
// 				EthSigner:     args[2],
// 				Orchestrator:  args[3],
// 				Signature:     str_signature,
// 			}
// 			// if err := msg.ValidateBasic(); err != nil {
// 			// 	return err
// 			// }

// 			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
// 		},
// 	}
// 	flags.AddTxFlagsToCmd(cmd)
// 	return cmd
// }

//MsgValsetConfirm
func CmdConfirmBatch() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "batch-confirm [nonce] [token-contract] [eth-signer] [orchestrator]",
		Short: "Confirm a specific batch",
		Args:  cobra.ExactArgs(4),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			//---------------------- 1. Verify args ----------------------

			//---------------------- 2. Convert nonce string into uint64 ----------------------
			nonceValue, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return sdkerrors.Wrap(err, "nonce conversion")
			}

			//---------------------- 3. Sign message ----------------------
			str_checkpoint := "be79157a6b00ea9e10d18a67c6c68cd888c3539850b26be790aa5258829f7616 "
			checkpoint := []byte(str_checkpoint)

			// queryClient := types.NewQueryClient(cliCtx)
			// req := &types.QueryCurrentValsetRequest{}
			// res, err := queryClient.CurrentValset(cmd.Context(), req)
			// if err != nil {
			// 	return err
			// }
			// valset := res.Valset
			// gravityId := "gravity-jemc"
			//in bytes: [103, 114, 97, 118, 105, 116, 121, 45, 106, 101, 109, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			//print as: "gravity-jemc\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}"

			// checkpoint := valset.GetCheckpoint(gravityId)

			// //transform eth privKey string into ecdsa key
			privKey_str := "1403bf5adde8a8085d2f872291fd9844071a122fb4f3f18b6f7565a3b81a6bc6"
			privKey, err := ethCrypto.HexToECDSA(privKey_str)
			if err != nil {
				log.Fatal(err)
			}
			//generate the eth signature
			ethSignature, err := types.NewEthereumSignature(checkpoint, privKey)
			if err != nil {
				log.Fatal(err)
			}
			// str_signature := hex.EncodeToString(ethSignature)
			str_signature := string(ethSignature)

			//verify signature
			types.ValidateEthereumSignature(checkpoint, ethSignature, "0x8196c0b8e4119eB91A4b69AD0522fD908d5f8E53")

			//---------------------- 4. Send msg ----------------------
			msg := types.MsgConfirmBatch{
				Nonce:         nonceValue,
				TokenContract: args[1],
				EthSigner:     args[2],
				Orchestrator:  args[3],
				Signature:     str_signature,
			}
			// if err := msg.ValidateBasic(); err != nil {
			// 	return err
			// }

			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}

//MsgValsetConfirm
func CmdValsetConfirm() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "valset-confirm [nonce] [orchestrator] [eth-address]",
		Short: "Confirm a specific validator set",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}

			//---------------------- 1. Verify args ----------------------

			//---------------------- 2. Convert nonce string into uint64 ----------------------
			nonceValue, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return sdkerrors.Wrap(err, "nonce conversion")
			}

			//---------------------- 3. Sign message ----------------------
			str_checkpoint := "be79157a6b00ea9e10d18a67c6c68cd888c3539850b26be790aa5258829f7616 "
			checkpoint := []byte(str_checkpoint)

			// queryClient := types.NewQueryClient(cliCtx)
			// req := &types.QueryCurrentValsetRequest{}
			// res, err := queryClient.CurrentValset(cmd.Context(), req)
			// if err != nil {
			// 	return err
			// }
			// valset := res.Valset
			// gravityId := "gravity-jemc"
			//in bytes: [103, 114, 97, 118, 105, 116, 121, 45, 106, 101, 109, 99, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
			//print as: "gravity-jemc\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}\u{0}"

			// checkpoint := valset.GetCheckpoint(gravityId)

			// //transform eth privKey string into ecdsa key
			privKey_str := "1403bf5adde8a8085d2f872291fd9844071a122fb4f3f18b6f7565a3b81a6bc6"
			privKey, err := ethCrypto.HexToECDSA(privKey_str)
			if err != nil {
				log.Fatal(err)
			}
			//generate the eth signature
			ethSignature, err := types.NewEthereumSignature(checkpoint, privKey)
			if err != nil {
				log.Fatal(err)
			}
			// str_signature := hex.EncodeToString(ethSignature)
			str_signature := string(ethSignature)

			//verify signature
			types.ValidateEthereumSignature(checkpoint, ethSignature, "0x8196c0b8e4119eB91A4b69AD0522fD908d5f8E53")

			//---------------------- 4. Send msg ----------------------
			msg := types.MsgValsetConfirm{
				Nonce:        nonceValue,
				Orchestrator: args[1],
				EthAddress:   args[2],
				Signature:    str_signature,
			}
			// if err := msg.ValidateBasic(); err != nil {
			// 	return err
			// }

			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}

// func makeValsetSignCmd() func(cmd *cobra.Command, args []string) error {
// 	signature := returnValidatorSign()

// }

// func returnValidatorSign() func(cmd *cobra.Command, args []string) []byte {
// 	return func(cmd *cobra.Command, args []string) (json []byte) {
// 		return json
// 	}
// }

func GetUnsafeTestingCmd() *cobra.Command {
	testingTxCmd := &cobra.Command{
		Use:                        "unsafe_testing",
		Short:                      "helpers for testing. not going into production",
		DisableFlagParsing:         true,
		SuggestionsMinimumDistance: 2,
		RunE:                       client.ValidateCmd,
	}
	testingTxCmd.AddCommand([]*cobra.Command{
		CmdUnsafeETHPrivKey(),
		CmdUnsafeETHAddr(),
	}...)

	return testingTxCmd
}

func CmdUnsafeETHPrivKey() *cobra.Command {
	return &cobra.Command{
		Use:   "gen-eth-key",
		Short: "Generate and print a new ecdsa key",
		RunE: func(cmd *cobra.Command, args []string) error {
			key, err := ethCrypto.GenerateKey()
			if err != nil {
				return sdkerrors.Wrap(err, "can not generate key")
			}
			k := "0x" + hex.EncodeToString(ethCrypto.FromECDSA(key))
			println(k)
			return nil
		},
	}
}

func CmdUnsafeETHAddr() *cobra.Command {
	return &cobra.Command{
		Use:   "eth-address",
		Short: "Print address for an ECDSA eth key",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			privKeyString := args[0][2:]
			privateKey, err := ethCrypto.HexToECDSA(privKeyString)
			if err != nil {
				log.Fatal(err)
			}
			// You've got to do all this to get an Eth address from the private key
			publicKey := privateKey.Public()
			publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
			if !ok {
				log.Fatal("error casting public key to ECDSA")
			}
			ethAddress := ethCrypto.PubkeyToAddress(*publicKeyECDSA).Hex()
			println(ethAddress)
			return nil
		},
	}
}

func CmdSendToEth() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "send-to-eth [eth-dest] [amount] [bridge-fee]",
		Short: "Adds a new entry to the transaction pool to withdraw an amount from the Ethereum bridge contract",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}
			cosmosAddr := cliCtx.GetFromAddress()

			amount, err := sdk.ParseCoinsNormalized(args[1])
			if err != nil {
				return sdkerrors.Wrap(err, "amount")
			}
			bridgeFee, err := sdk.ParseCoinsNormalized(args[2])
			if err != nil {
				return sdkerrors.Wrap(err, "bridge fee")
			}

			if len(amount) > 1 || len(bridgeFee) > 1 {
				return fmt.Errorf("coin amounts too long, expecting just 1 coin amount for both amount and bridgeFee")
			}

			// Make the message
			msg := types.MsgSendToEth{
				Sender:    cosmosAddr.String(),
				EthDest:   args[0],
				Amount:    amount[0],
				BridgeFee: bridgeFee[0],
			}
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			// Send it
			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}

func CmdRequestBatch() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "build-batch [token_contract_address]",
		Short: "Build a new batch on the cosmos side for pooled withdrawal transactions",
		Args:  cobra.ExactArgs(1),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}
			cosmosAddr := cliCtx.GetFromAddress()

			// TODO: better denom searching
			msg := types.MsgRequestBatch{
				Sender: cosmosAddr.String(),
				// Denom:  fmt.Sprintf("gravity%s", args[0]),
				Denom: args[0],
			}
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			// Send it
			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}

func CmdSetOrchestratorAddress() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "set-orchestrator-address [validator-address] [orchestrator-address] [ethereum-address]",
		Short: "Allows validators to delegate their voting responsibilities to a given key.",
		Args:  cobra.ExactArgs(3),
		RunE: func(cmd *cobra.Command, args []string) error {
			cliCtx, err := client.GetClientTxContext(cmd)
			if err != nil {
				return err
			}
			msg := types.MsgSetOrchestratorAddress{
				Validator:    args[0],
				Orchestrator: args[1],
				EthAddress:   args[2],
			}
			if err := msg.ValidateBasic(); err != nil {
				return err
			}
			// Send it
			return tx.GenerateOrBroadcastTxCLI(cliCtx, cmd.Flags(), &msg)
		},
	}
	flags.AddTxFlagsToCmd(cmd)
	return cmd
}
